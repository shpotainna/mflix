package mflix.api.daos;

import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

import static com.mongodb.client.MongoClients.create;
import static com.mongodb.client.model.Filters.not;
import static com.mongodb.client.model.Filters.type;

@SpringBootTest
public class MigrationTest extends TicketTest {
    MongoCollection<Document> movies;

    @Before
    public void setup() throws IOException {
        String mongoUri = getProperty("spring.mongodb.uri");
        movies = create(mongoUri)
                .getDatabase("mflix")
                .getCollection("movies");
    }

    @Test
    public void testAllDocumentsUpdateDateIsDateType() {
        Bson filter = type("lastupdated", "string");

        int expectedCount = 0;
        long actualCount = movies.countDocuments(filter);
        Assert.assertEquals(
                "Should not find documents where `lastupdated` is of " + "`string` type",
                expectedCount,
                actualCount);
    }

    @Test
    public void testAllDocumentsIMDBRatingNumber() {
        Bson filter = not(type("imdb.rating", "number"));

        int expectedCount = 0;
        Assert.assertEquals(
                "Should not find documents where `imdb.rating` is of" + " not of `number` type",
                expectedCount,
                movies.countDocuments(filter));
    }
}
