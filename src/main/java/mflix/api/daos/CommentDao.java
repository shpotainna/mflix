package mflix.api.daos;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import mflix.api.models.Comment;
import mflix.api.models.Critic;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.mongodb.MongoClientSettings.getDefaultCodecRegistry;
import static com.mongodb.ReadConcern.MAJORITY;
import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Sorts.descending;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;
import static org.slf4j.LoggerFactory.getLogger;

@Component
public class CommentDao extends AbstractMFlixDao {

    public static String COMMENT_COLLECTION = "comments";

    private MongoCollection<Comment> commentCollection;

    private CodecRegistry pojoCodecRegistry;

    private final Logger log;

    @Autowired
    public CommentDao(
            MongoClient mongoClient,
            @Value("${spring.mongodb.database}") String databaseName
    ) {
        super(mongoClient, databaseName);
        log = getLogger(this.getClass());
        this.db = this.mongoClient.getDatabase(MFLIX_DATABASE);
        this.pojoCodecRegistry = fromRegistries(
                getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build())
        );
        this.commentCollection = db
                .getCollection(COMMENT_COLLECTION, Comment.class)
                .withCodecRegistry(pojoCodecRegistry);
    }

    /**
     * Returns a Comment object that matches the provided id string.
     *
     * @param id - comment identifier
     * @return Comment object corresponding to the identifier value
     */
    public Comment getComment(String id) {
        Document document = new Document("_id", new ObjectId(id));
        return commentCollection
                .find(document)
                .first();
    }

    /**
     * Adds a new Comment to the collection. The equivalent instruction in the mongo shell would be:
     *
     * <p>
     * db.comments.insertOne({comment})
     * <p>
     *
     * @param comment - Comment object.
     * @throw IncorrectDaoOperation if the insert fails, otherwise
     * returns the resulting Comment object.
     */
    public Comment addComment(Comment comment) {
        if (comment.getId() == null || comment.getId().isEmpty()) {
            throw new IncorrectDaoOperation("Comment objects need to have an id field set.");
        }
        commentCollection.insertOne(comment);
        return comment;
    }

    /**
     * Updates the comment text matching commentId and user email. This method would be equivalent to
     * running the following mongo shell command:
     *
     * <p>
     * db.comments.update({_id: commentId}, {$set: { "text": text, date: ISODate() }})
     * <p>
     *
     * @param commentId - comment id string value.
     * @param text      - comment text to be updated.
     * @param email     - user email.
     * @return true if successfully updates the comment text.
     */
    public boolean updateComment(String commentId, String text, String email) {
        Bson filter = and(
                eq("email", email),
                eq("_id", new ObjectId(commentId))
        );
        Bson update = combine(
                set("text", text),
                set("date", new Date())
        );
        UpdateResult result = commentCollection.updateOne(filter, update);
        if (result.getMatchedCount() > 0) {
            if (result.getModifiedCount() != 1) {
                log.warn("Comment `{}` text was not updated. Is it the same text?");
            }
            return true;
        }
        log.error(
                "Could not update comment `{}`. Make sure the comment is owned by `{}`",
                commentId,
                email
        );
        return false;
    }

    /**
     * Deletes comment that matches user email and commentId.
     *
     * @param commentId - commentId string value.
     * @param email     - user email value.
     * @return true if successful deletes the comment.
     */
    public boolean deleteComment(String commentId, String email) {
        Bson filter = and(
                eq("email", email),
                eq("_id", new ObjectId(commentId))
        );
        DeleteResult result = commentCollection.deleteOne(filter);
        if (result.getDeletedCount() != 1) {
            log.warn(
                    "Not able to delete comment `{}` for user `{}`. User does not own comment or already deleted!",
                    commentId,
                    email
            );
            return false;
        }
        return true;
    }

    /**
     * Ticket: User Report - produce a list of users that comment the most in the website. Query the
     * `comments` collection and group the users by number of comments. The list is limited to up most
     * 20 commenter.
     *
     * @return List {@link Critic} objects.
     */
    public List<Critic> mostActiveCommenters() {
        List<Critic> mostActive = new ArrayList<>();
        Bson groupByCountStage = sortByCount("$email");
        Bson sortStage = sort(descending("count"));
        Bson limitStage = limit(20);
        List<Bson> pipeline = new ArrayList<>();
        pipeline.add(groupByCountStage);
        pipeline.add(sortStage);
        pipeline.add(limitStage);

        MongoCollection<Critic> commentCriticCollection =
                this.db.getCollection("comments", Critic.class)
                        .withCodecRegistry(this.pojoCodecRegistry)
                        .withReadConcern(MAJORITY);

        commentCriticCollection.aggregate(pipeline).into(mostActive);
        return mostActive;
    }
}
