package mflix.api.daos;

import com.mongodb.MongoClientSettings;
import com.mongodb.MongoException;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import mflix.api.models.Session;
import mflix.api.models.User;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

import static com.mongodb.WriteConcern.MAJORITY;
import static com.mongodb.client.model.Updates.set;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;
import static org.slf4j.LoggerFactory.getLogger;

@Configuration
public class UserDao extends AbstractMFlixDao {
    public static String USER_COLLECTION = "users";
    public static String SESSION_COLLECTION = "sessions";

    private final MongoCollection<User> usersCollection;
    private final MongoCollection<Session> sessionsCollection;
    private final Logger log;

    @Autowired
    public UserDao(
            MongoClient mongoClient, @Value("${spring.mongodb.database}") String databaseName) {
        super(mongoClient, databaseName);
        CodecRegistry pojoCodecRegistry =
                fromRegistries(
                        MongoClientSettings.getDefaultCodecRegistry(),
                        fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        usersCollection = db.getCollection(USER_COLLECTION, User.class)
                .withCodecRegistry(pojoCodecRegistry);
        log = getLogger(this.getClass());
        sessionsCollection = db.getCollection(SESSION_COLLECTION, Session.class)
                .withCodecRegistry(pojoCodecRegistry);
    }

    /**
     * Inserts the `user` object in the `users` collection.
     *
     * @param user - User object to be added
     * @return True if successful, throw IncorrectDaoOperation otherwise
     */
    public boolean addUser(User user) {
        try {
            usersCollection
                    .withWriteConcern(MAJORITY)
                    .insertOne(user);
            return true;
        } catch (MongoException exception) {
            log.error("MongoException occurred in addUser", exception);
            throw new IncorrectDaoOperation(
                    "MongoException occurred in addUser",
                    exception
            );
        }
    }

    /**
     * Creates session using userId and jwt token.
     *
     * @param userId - user string identifier
     * @param jwt    - jwt string token
     * @return true if successful
     */
    public boolean createUserSession(String userId, String jwt) {
        Bson userBson = new Document("user_id", userId);
        Bson setJwt = set("jwt", jwt);
        UpdateOptions options = new UpdateOptions().upsert(true);
        try {
            sessionsCollection.updateOne(userBson, setJwt, options);
            return true;
        } catch (MongoException exception) {
            log.error("MongoException occurred in addUser", exception);
            throw new IncorrectDaoOperation(
                    "MongoException occurred in createUserSession",
                    exception
            );
        }
    }

    /**
     * Returns the User object matching the an email string value.
     *
     * @param email - email string to be matched.
     * @return User object or null.
     */
    public User getUser(String email) {
        Document document = new Document("email", email);
        return usersCollection
                .find(document)
                .limit(1)
                .first();
    }

    /**
     * Given the userId, returns a Session object.
     *
     * @param userId - user string identifier.
     * @return Session object or null.
     */
    public Session getUserSession(String userId) {
        Document document = new Document("user_id", userId);
        return sessionsCollection
                .find(document)
                .limit(1)
                .first();
    }

    public boolean deleteUserSessions(String userId) {
        Document document = new Document("user_id", userId);
        DeleteResult result = sessionsCollection.deleteOne(document);
        if (result.getDeletedCount() < 1) {
            log.warn("User `{}` could not be found in sessions collection.", userId);
        }
        return result.wasAcknowledged();
    }

    /**
     * Removes the user document that match the provided email.
     *
     * @param email - of the user to be deleted.
     * @return true if user successfully removed
     */
    public boolean deleteUser(String email) {
        if (deleteUserSessions(email)) {
            Document userDeleteFilter = new Document("email", email);
            DeleteResult result = usersCollection.deleteOne(userDeleteFilter);
            if (result.getDeletedCount() < 0) {
                log.warn("User with `email` {} not found. Potential concurrent operation?!");
            }
            return result.wasAcknowledged();
        }
        return false;
    }

    /**
     * Updates the preferences of an user identified by `email` parameter.
     *
     * @param email           - user to be updated email
     * @param userPreferences - set of preferences that should be stored and replace the existing
     *                        ones. Cannot be set to null value
     * @return User object that just been updated.
     */
    public boolean updateUserPreferences(String email, Map<String, ?> userPreferences) {
        if (userPreferences == null) {
            throw new IncorrectDaoOperation(
                    "User preferences cannot be set to null");
        }
        Bson filter = new Document("email", email);
        Bson object = set("preferences", userPreferences);
        UpdateResult res = usersCollection.updateOne(filter, object);
        if (res.getModifiedCount() < 1) {
            log.warn("User `{}` was not updated. Trying to re-write the same `preferences` field: `{}`",
                    email, userPreferences);
        }
        return true;
    }
}
